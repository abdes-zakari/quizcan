import { Component } from '@angular/core';
import {HttpClient, HttpHeaders } from '@angular/common/http';
import {interval, Observable} from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: 'main.page.html',
  styleUrls: ['main.page.scss']
})
export class MainPage {

	questions:any;
	start:boolean=false;
	currentQuestion:any;
	currentIndex:any=0;
	successBox:any=false;
	wrongBox:any=false;
	score:any=0;
	answerd:any=false;
	endQuiz:any=false;
	home:any=true;
  optionsImgTrue:any=[false,false,false,false];
  optionsImgFalse:any=[false,false,false,false];
  loadQuestion:any=false;
  timerWidth:any=100;
  varWidth:any="100%";
  timero:any=false;
  musicTimer:any=null;
  soundMute:any=false;

	constructor(private http: HttpClient) {}

	ngOnInit(){
        this.http.get('https://api.myjson.com/bins/1gqiqk')
          .subscribe( data => {
          this.questions = data;
          //this.startQuiz();
          // console.log('data here:'+data)
          // console.log(data);
        });
        // if (true) {}

        // if (this.timero == true) {
        //   console.log('sdsadsads');
        //     setTimeout(() => { this.timer();}, 1000);
        // } 

        interval(1000).subscribe((x) => {
          console.log(this.timero);
          if (this.timero===true) {
                  this.timer();
            }
        });
        
        this.timerMusic();

    }



    startQuiz(){
       
       this.home = false;
       this.start = true;
       this.loadQuestion = true;
       this.timero = true;
       this.musicTimer.play();
       
       
   
       this.currentQuestion = this.questions[this.currentIndex];
       // console.log(this.questions[0]);


    }

    nextQuestion(){

        this.timerWidth=100;
        this.varWidth="100%";
        this.timero = false;
        this.answerd = false;
        this.successBox = false;
        this.wrongBox = false;
        this.optionsImgFalse = [false,false,false,false];
        this.optionsImgTrue = [false,false,false,false];
        this.loadQuestion = true;

    	// console.log(this.currentIndex+' VS '+this.questions.length);
      if (this.home == false) {
      	if (this.currentIndex < this.questions.length-1) {
              
         		this.currentIndex = this.currentIndex+1
         		this.currentQuestion = this.questions[this.currentIndex];
            this.timero = true;
            this.musicTimer.play();
      	}else{
      		console.log('Quiz is over');
      		this.quizIsOver();
      	}
      	// console.log(this.currentIndex);
      }
    }

    handleAnswer(index){

     // console.log(index);
     // console.log(this.currentQuestion.options[index]);
     if (this.answerd == false) {
     	let chosedAnswer = index;
     	if (chosedAnswer == this.currentQuestion.answer) {
     	   this.successBox = true;
     	   this.score = this.score + 10;
     	}else{
     		this.wrongBox = true;
     	}
     	this.answerd = true;
     }

    }

    handleAnswer2(index){
     this.stopMusicTimer();
     console.log(index);
     console.log(this.currentQuestion.options[index]);
     this.timero = false;
      if (this.answerd == false) {
        let chosedAnswer = index;
        if (chosedAnswer == this.currentQuestion.answer) {
           // this.successBox = true;
           this.score = this.score + 10;
           this.playAudio("correct");
           this.optionsImgTrue[index] = true; 

        }else{
          // this.wrongBox = true;
          this.playAudio("wrong");
          this.optionsImgFalse[index] = true;
          this.optionsImgTrue[this.currentQuestion.answer] = true;
        }
        this.loadQuestion = false;
        this.answerd = true;
        setTimeout(() => { this.nextQuestion(); }, 2000);
      }


    }

    quizIsOver(){

    	this.start = false;
    	this.endQuiz = true;
      this.stopMusicTimer();
  
    }

    playAgain(){
        
        this.endQuiz = false;
       	this.score = 0;
       	this.currentIndex = 0;
    	  this.successBox = false;
		    this.wrongBox = false;
		    this.answerd = false;
        this.loadQuestion = false;
    	  this.startQuiz();
    }



    playAudio(sound){
      let audio = new Audio();
      audio.src = "../../assets/audio/"+sound+".mp3";
      if (this.soundMute == false) {

          audio.load();
          audio.play();
      }
    }

    timerMusic(){
      console.log('timerMusic');
      this.musicTimer = new Audio();
      this.musicTimer.src = "../../assets/audio/timer.mp3";
    }

    timer(){
      // timerWidth

      // setTimeout(() => { 
      //   this.timerWidth = this.timerWidth - 5;
      //   this.varWidth = this.timerWidth+"%";
      // }, 1000);
        if (this.timerWidth > 0) {

            this.timerWidth = this.timerWidth - 6.25;
            this.varWidth = this.timerWidth+"%";
        }

        if (this.timerWidth == 0 ) {
          this.timero = false;
          this.answerd = true;
          this.stopMusicTimer();
          setTimeout(() => { this.nextQuestion(); }, 1000);
        }
    }


    stopMusicTimer(){

       this.musicTimer.pause();
       this.musicTimer.currentTime = 0;
    }


    goHome(){
      this.home = true;
      this.start = false;
      this.endQuiz = false;
      this.score = 0;
      this.currentIndex = 0;
      this.answerd = false;
      this.loadQuestion = false;
      this.stopMusicTimer();
      this.timerWidth=100;
      this.varWidth="100%";
      this.timero = false;

    }

    toggelMuteSound(){

      this.soundMute = !this.soundMute;

      if (this.soundMute == true) {
         this.musicTimer.volume = 0;
      }else{
         this.musicTimer.volume = 1;
      }
    }
}

